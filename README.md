Adrien CROSIO
Thomas DAMASCENO
# Projet Domotique

Le but du projet est de réaliser un système domotique pour controller la vanne d'aération d'une pièce en fonction de la température ambiante.

Le matériel que nous avons utiliser est le suivant :
- Un Thermomètre bluetooth grand public
- Un esp32 pour récupérer les données du thermomètre
- Un PC faisant office de broker MQTT
- Une carte STM32 pour controler le système d'aération à l'aide d'un servomoteur

## Nos conseils pour bien démarrer

- Prévoir un PC en plus du PC fix de CPE. Ils est impossible de lancer les projects d'exemple nécessaire à l'utilisation de la carte STM32 sur le PC de CPE. A cause de StmCubeIDE
- Faire l'installation de StmCubeIDE sur votre PC, si vous etes sous Windows, executer l'IDE en admin, sinon le logiciel ne pourras pas télécharger les paquets nécessaire au code d'exemple
- Pour le broker MQTT Mosquitto, si vous le lancez avec le PC de CPE, il y a dejà une instance de Mosquitto qui tourne sur le port par défaut (1883) cependant il n'est pas accéssible, donc vous devez lancer un autre borker sur un port libre.
- Lancer le broker MQTT en full verbsose pour avoir tout les log et pouvoir debuger.
- Eviter d'utiliser des applications android douteuse pour tester votre broker Mosquitto.

## Réalisation du récupérateur de donnée sur ESP32

### Préparation de l'environnement de développement

Pour programmer votre carte ESP32 nous conseillons l'utilisation de Visual Studio Code avec le plugin PlatformIO, celui ci permettant d'interfacer facilement avec le matériel et de télécharger les bibliothèques adaptées. De plus ce plugin est moins lourd que le plugin ESP officiel, ce qui peut épargner votre espace mémoire (particulièrement sur les PC de CPE).

### Récupération du nom du device (Thermometre)

La premiere chose a faire est de lister tout les appareils bluetooth que vous pouvez détecter pour ensuite les filtrer et trouver le bon device.

Methode de filtrage:
- Distance (s'eloigner dans un coin isoler avec le thermometre), la plupart des applications permettant de filtrer les devices par force de signal.

### Récupération des données en C++

Une fois que vous connaissez le nom de votre device il devient aisé de filtrer la liste des appareils détecté dans votre code afin de récupérer uniquement les données de l'appareil qui vous intéresse.

Dans le cas de notre thermometre, celui ci transmet la température périodiquement dans sa trame d'advertising. Aucune documentation n'existe pour la structure de la trame, celle ci n'étant pas standard, il vous faudra donc un peu de patience pour déterminer quels octets contiennent la donnée de température.
(Attention, toutes les trames d'advertising ne contiennent pas la température, il existe un octet indiquant le type de trame qui peux vous aider).

### Transmission des données via MQTT 

Une fois que vous avez isolé la température il ne vous reste plus qu'à la formater afin de l'envoyer à votre broker MQTT.
Il existe un ensemble de bibliothèque MQTT pour ESP32 sur PlatformIO qui vous rendront la tache très aisé.
Si jamais vous manquer de place sur votre ESP32 pour faire rentrer la totalité de votre application, pensez à changer la variable d'environnement suivante dans le fichier platformio.ini.
```ini
board_build.partitions = huge_app.csv 
```

## Client MQTT  avec la carte Discovery STM32

Attention : pour éviter les maux de tête, nous vous conseillons fortement d'utiliser les codes d'exemples **OFFICIELS** des bibliothèques que vous utilisez avec les cartes STM32. En effet, certains codes trouvés sur GitHub peuvent sembler marcher au premier coup d'oeil mais feront planter votre carte sans votre consentement.

Par exemple pour votre client MQTT nous vous recommandons la bibliothèque et le code d'exemple suivants : https://www.nongnu.org/lwip/2_0_x/group__mqtt.html

## Contrôle du servomoteur

Après avoir perdu 8h sur le setup de l'environnement et sur les crash de la carte à cause de mauvais codes d'exemple, nous n'avons pas eu le temps de nous pencher sur le controle du servomoteur.
