#include <Arduino.h>

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>

#include <EspMQTTClient.h>

EspMQTTClient client(
  "B127-EIC",
  "b127-eic",
  "192.168.1.100",  // MQTT Broker server ip
  NULL,   // Can be omitted if not needed
  NULL,   // Can be omitted if not needed
  "TestClient",      // Client name that uniquely identify your device
  1884
);

int scanTime = 5; //In seconds
BLEScan* pBLEScan;

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      if(advertisedDevice.getName() == "MJ_HT_V1"){
        // Serial.printf("Advertised Device: %s \n", advertisedDevice.toString().c_str());
        uint8_t * ptr = advertisedDevice.getPayload();
        // for (int i = 0; i < advertisedDevice.getPayloadLength(); i++)
        //   Serial.printf("%02X ", *(ptr + i));
        // Serial.printf("\n");
        int calc = ptr[22] * 256 + ptr[21];
        if(ptr[18] == 0x0D){
          char res[10] = { 0 };
          sprintf(res, "%d.%d", calc / 10, calc % 10);
          Serial.printf("Température : %s °C",  res);
          client.publish("temp", res);
        }
        Serial.printf("\n");
      }
    } 
};

void onConnectionEstablished() {

  client.subscribe("temp", [] (const String &payload)  {
    Serial.println(payload);
  });
}

void setup() {
  Serial.begin(460800);
  Serial.println("Scanning...");

  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan(); //create new scan
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);  // less or equal setInterval value
}

void loop() {
  client.loop();
  // put your main code here, to run repeatedly:
  BLEScanResults foundDevices = pBLEScan->start(scanTime, false);
  // Serial.print("Devices found: ");
  // Serial.println(foundDevices.getCount());
  // Serial.println("Scan done!");
  pBLEScan->clearResults();   // delete results fromBLEScan buffer to release memory
  delay(2000);
}